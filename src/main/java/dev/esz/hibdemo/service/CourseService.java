package dev.esz.hibdemo.service;

import dev.esz.hibdemo.model.Course;
import dev.esz.hibdemo.repository.CourseRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class CourseService {
    private CourseRepository courseRepository;

    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public void deleteAllCourses() {
        courseRepository.deleteAllInBatch();
    }

    public void insertCourseBatch(Set<Course> courseSet) {
        courseRepository.saveAll(courseSet);
    }

    public List<Course> getAllCourses() {
        return courseRepository.findAll();
    }
}
