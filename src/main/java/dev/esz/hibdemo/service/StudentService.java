package dev.esz.hibdemo.service;

import dev.esz.hibdemo.model.Student;
import dev.esz.hibdemo.repository.StudentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    private StudentRepository studentRepository;

    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public List<Student> getStudentById(Long id) {
        return studentRepository.findStudentById(id);
    }

    public List<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    public Student insertStudent(Student student) {
        return studentRepository.save(student);
    }

    public void insertStudentBatch(List<Student> students) {
        studentRepository.saveAll(students);
    }

    public void deleteAllStudent() {
        studentRepository.deleteAllInBatch();
    }
}
