package dev.esz.hibdemo.controller;

import dev.esz.hibdemo.converter.StudentDtoConverter;
import dev.esz.hibdemo.dto.StudentDto;
import dev.esz.hibdemo.model.Student;
import dev.esz.hibdemo.service.StudentService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/students")
public class StudentController {
    private StudentService studentService;
    private StudentDtoConverter studentDtoConverter;

    public StudentController(StudentService studentService, StudentDtoConverter studentDtoConverter) {
        this.studentService = studentService;
        this.studentDtoConverter = studentDtoConverter;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<StudentDto> getAllStudents() {
        List<Student> allStudents =  studentService.getAllStudents();
        return allStudents.stream().map(studentDtoConverter::convertToDto).collect(Collectors.toList());
    }
}
