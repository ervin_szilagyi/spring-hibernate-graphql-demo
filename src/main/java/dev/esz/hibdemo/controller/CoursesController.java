package dev.esz.hibdemo.controller;

import dev.esz.hibdemo.converter.CourseDtoConverter;
import dev.esz.hibdemo.converter.StudentDtoConverter;
import dev.esz.hibdemo.dto.CourseDto;
import dev.esz.hibdemo.dto.CourseDtoWithStudents;
import dev.esz.hibdemo.model.Course;
import dev.esz.hibdemo.service.CourseService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/courses")
public class CoursesController {
    private CourseService courseService;
    private CourseDtoConverter courseDtoConverter;

    public CoursesController(CourseService courseService, CourseDtoConverter courseDtoConverter) {
        this.courseService = courseService;
        this.courseDtoConverter = courseDtoConverter;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CourseDtoWithStudents> getCourses() {
        return courseService.getAllCourses().stream()
                .map(courseDtoConverter::convertToCourseDtoWithStudents).collect(Collectors.toList());
    }
}
