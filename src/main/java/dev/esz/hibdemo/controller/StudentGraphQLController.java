package dev.esz.hibdemo.controller;

import dev.esz.hibdemo.service.GraphQLService;
import graphql.ExecutionResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/graphql")
public class StudentGraphQLController {
    private GraphQLService graphQLService;

    public StudentGraphQLController(GraphQLService graphQLService) {
        this.graphQLService = graphQLService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> query() {
        ExecutionResult execute = graphQLService.getGraphQL().execute("{ allStudents { id firstName lastName } }");
        return new ResponseEntity<>(execute, HttpStatus.OK);
    }
}
