package dev.esz.hibdemo.converter;

import dev.esz.hibdemo.dto.StudentDto;
import dev.esz.hibdemo.model.Student;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class StudentDtoConverter {
    private ModelMapper modelMapper;

    public StudentDtoConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public Student convertFromDto(StudentDto studentDto) {
        return modelMapper.map(studentDto, Student.class);
    }

    public StudentDto convertToDto(Student student) {
        return modelMapper.map(student, StudentDto.class);
    }
}
