package dev.esz.hibdemo.converter;

import dev.esz.hibdemo.dto.CourseDto;
import dev.esz.hibdemo.dto.CourseDtoWithStudents;
import dev.esz.hibdemo.model.Course;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class CourseDtoConverter {
    private ModelMapper modelMapper;

    public CourseDtoConverter(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public Course convertFromDto(CourseDto courseDto) {
        return modelMapper.map(courseDto, Course.class);
    }

    public CourseDto convertToCourseDto(Course course) {
        return modelMapper.map(course, CourseDto.class);
    }

    public Course convertFromDto(CourseDtoWithStudents courseDto) {
        return modelMapper.map(courseDto, Course.class);
    }

    public CourseDtoWithStudents convertToCourseDtoWithStudents(Course course) {
        return modelMapper.map(course, CourseDtoWithStudents.class);
    }
}
