package dev.esz.hibdemo;

import dev.esz.hibdemo.converter.CourseDtoConverter;
import dev.esz.hibdemo.converter.StudentDtoConverter;
import dev.esz.hibdemo.dto.CourseDto;
import dev.esz.hibdemo.dto.StudentDto;
import dev.esz.hibdemo.model.Course;
import dev.esz.hibdemo.model.Student;
import dev.esz.hibdemo.service.StudentService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
public class HibdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HibdemoApplication.class, args);
    }

    @Bean
    public CommandLineRunner initialize(StudentService studentService, StudentDtoConverter studentDtoConverter,
                                        CourseDtoConverter courseDtoConverter) {
        return args -> {
            CourseDto algebra = CourseDto.builder().name("algebra").build();
            CourseDto cs = CourseDto.builder().name("computer science").build();
            CourseDto programming = CourseDto.builder().name("programming").build();
            CourseDto dm = CourseDto.builder().name("discrete math").build();
            CourseDto stats = CourseDto.builder().name("statistic").build();
            CourseDto ds = CourseDto.builder().name("data science").build();
            List<Course> courses = Stream.of(algebra, cs, programming, dm, stats, ds)
                    .map(courseDtoConverter::convertFromDto).collect(Collectors.toList());
            StudentDto todd = StudentDto.builder().firstName("Todd").lastName("Priscilla").year(3).build();
            StudentDto london = StudentDto.builder().firstName("London").lastName("Jack").year(2).build();
            StudentDto suarez = StudentDto.builder().firstName("Suarez").lastName("Daniel").year(4).build();
            List<Student> students = Stream.of(todd, london, suarez)
                    .map(studentDtoConverter::convertFromDto).collect(Collectors.toList());
            students.get(0).getCourses().addAll(Arrays.asList(courses.get(0), courses.get(1)));
            students.get(1).getCourses().addAll(Arrays.asList(courses.get(3), courses.get(4), courses.get(5)));
            students.get(2).getCourses().addAll(Arrays.asList(courses.get(0), courses.get(3), courses.get(5)));
            studentService.insertStudentBatch(students);
        };
    }

}

