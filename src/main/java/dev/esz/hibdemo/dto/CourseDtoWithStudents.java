package dev.esz.hibdemo.dto;

import lombok.*;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class CourseDtoWithStudents {
    private Long id;
    private String name;
    private @Singular Set<StudentDto> students;
}
