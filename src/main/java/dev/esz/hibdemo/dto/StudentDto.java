package dev.esz.hibdemo.dto;

import lombok.*;

import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class StudentDto {
    private Long id;
    private String firstName;
    private String lastName;
    private Integer year;
    private @Singular Set<CourseDto> courses;
}
