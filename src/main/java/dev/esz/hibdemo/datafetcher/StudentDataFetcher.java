package dev.esz.hibdemo.datafetcher;

import dev.esz.hibdemo.model.Student;
import dev.esz.hibdemo.repository.StudentRepository;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StudentDataFetcher implements DataFetcher<List<Student>> {
    private StudentRepository studentRepository;

    public StudentDataFetcher(StudentRepository studentRepository) {
        this.studentRepository =  studentRepository;
    }

    @Override
    public List<Student> get(DataFetchingEnvironment dataFetchingEnvironment) {
        return studentRepository.findAll();
    }
}
