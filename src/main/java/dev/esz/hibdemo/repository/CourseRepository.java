package dev.esz.hibdemo.repository;

import dev.esz.hibdemo.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
