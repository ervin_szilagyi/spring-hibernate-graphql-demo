package dev.esz.hibdemo.repository;

import dev.esz.hibdemo.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {
    List<Student> findStudentById(Long id);
}
