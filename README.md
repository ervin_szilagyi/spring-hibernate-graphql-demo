# spring-hibernate-graphql-demo

As the title says this a Spring demo project on configuring Hibernate and graphql.

## Building the application
```
mvn package
```

## Setting up postgres and adminer
```
docker-compose up
``` 
Adminer can be reached on `localhost:8888`

## Tearing down postgres and adminer
```
docker-compose down -v
```